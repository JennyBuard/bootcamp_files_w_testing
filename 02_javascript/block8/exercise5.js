var sorter = (arr, order) => {
	if(order){
		if(order != 'ascending' && order != 'descending'){
			return  (`wrong order provided ${order} please provide us with ascending or descending order instructions`)
		}	
	}
	var orderAscending = ()=>{
		if(arr[i] > arr[j]){
			var temp = arr[i]
			arr[i]   = arr[j], arr[j]   = temp
		}
	},
	orderDiscending = () => {
		if(arr[i] < arr[j]){
			var temp = arr[j]
			arr[j]   = arr[i], arr[i]   = temp
		}
	}
	for (var i =0; i< arr.length; i++){
		for(var j =i+1;j<arr.length;j++ ){
			order == 'ascending' || !order ? orderAscending() :orderDiscending()
    	}
	}
	return arr
}

module.exports ={
    sorter
}
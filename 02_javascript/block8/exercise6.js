var Server = function(arg){
    var self = this;
    arg = arg || -1
    this.app = {
        get: function (params){
            var action  = params.split('/')[1], 
            name    = params.split('/')[2],
            amount  = params.split('/')[3];
            if( action == 'sort'){
                var by = name, order = amount;
            }
            this.actions = {
                find:(name)=> {   
                    if(name){
                        return self.accounts.filter( a => a.id == name)[0]
                    }
                    else {
                       return self.accounts
                    }
                },
                sort: (by)=>{
                    if(!by || by == 'name') {
                        if(order == 'asc' ) return self.accounts.sort((a,b) => a.name.localeCompare(b.name))
                        if(order == 'desc') return self.accounts.sort((a,b) => b.name.localeCompare(a.name))
                    }else{
                        if(order == 'asc' ) return self.accounts.sort(a,b  => a.amount -b.amount  )
                        if(order == 'desc') return self.accounts.sort((a,b)=> b.amount - a.amount )
                    }
                }
                
            }
            if(this.actions[action]){
                return this.actions[action](name)
            }
            else return '404 page not found'
            
        },
        post:function(params){

            var noIndex = () => "Account not found"
            var action  = params.split('/')[1].trim(), 
                name    = params.split('/')[2].trim(),
                amount  = Number(params.split('/')[3]),
                newName = params.split('/')[4];

                var criteria;
            !isNaN(name) && action == 'update' || !isNaN(name) && action == 'delete' ? criteria = 'id' : criteria = 'name'
            var index      = self.accounts.findIndex(a => a[criteria] == name);
            this.actions = {
                new:    () => {
                    arg++
                    if(index == -1){
                        self.accounts.push({name, amount, id:arg})
                    }
                    else{
                        return `Account ${name} already present in db`
                    }
                },
                delete: () => {
                  if  (self.accounts[index]) {
                      self.accounts.splice(index, 1)
                  } else{
                      return noIndex()
                  }
                },
                update: () => {
                    console.log('amount', amount)
                    if(self.accounts[index]){  
                        self.accounts[index]['name']   = newName
                        self.accounts[index]['amount'] = amount
                    }else{
                        noIndex()
                    }
                },
                withdraw: (index, val) =>{
                    if(self.accounts[index]) return self.accounts[index].amount -= val 
                    else return noIndex()
                },
                deposit:  (index, val) =>{
                    if(self.accounts[index]) return self.accounts[index].amount += val 
                    else return  noIndex()
                }
            }
            return this.actions[action](name, amount)
        }
    };
    this.accounts =[]
}


module.exports = {
    Server
}


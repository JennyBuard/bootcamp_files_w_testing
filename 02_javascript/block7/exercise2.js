var addCurrency = (coin, value, crypt) => {
    var index = crypt.findIndex(a => a.coin == coin.coin);
    if(index == -1) {
        crypt.push(coin) 
        var { coin } = coin
        var moneta   = coin.substring(0,1).toUpperCase()+coin.substring(1,coin.length);
        return `New coin ${moneta} added to Database`
	}
	else return findcurrency(coin, value, index, crypt);
},
findcurrency = (coin, value, index, crypt) => {
    var rate = crypt[index].rate 
    return converter(value, rate, coin)
    
},
converter = (value, rate, coin) =>{
    var result;
    rate > 0  ? result = value * rate : rate = value / rate
    return tellConversion(result, coin, value)
},
tellConversion = (result, coin, value)=>{
    return `You will receive ${result} usd for your ${value} ${coin.coin}s`
}
module.exports = {
    addCurrency, findcurrency, converter, tellConversion
}
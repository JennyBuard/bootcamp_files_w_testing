var checker = str => {
    let commas = [], questionmarks = []
    str.split('').map(ele => {
        if(ele == "?") questionmarks.push(ele)
        if(ele == ",") commas.push(ele)
    })
    return `${commas.length} ${commas.length > 1 ? "commas" : "comma"} ${questionmarks.length} ${questionmarks.length > 1 ?  "question marks" : "question mark"}`
}

module.exports = {
    checker
}



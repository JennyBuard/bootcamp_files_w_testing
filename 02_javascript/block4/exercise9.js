var uniqueElements = (arr, max) =>{
    var newArr = []
    arr.map(ele=>{
        !newArr.includes(ele) && typeof ele == 'number' && ele > max ? newArr.push(ele) : null
    })
    return `old array ${arr} \n new array ${newArr}`
}
module.exports = {
    uniqueElements
}
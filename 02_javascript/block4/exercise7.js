var boolanChecker = (arr, max = Infinity) =>{
    let bools= []
    arr.map(ele =>{
        typeof ele == 'boolean' && bools.length < max ? bools.push(ele) : null
    })
    return `${bools.length} booleans were found ${bools}`
}
module.exports = {
    boolanChecker
}
var check_types = a => {
    let types = []
    a.map( el => {
        !types.includes(typeof el) ? types.push(typeof el) :null
    })
    return types.length
} 

module.exports ={
    check_types
}
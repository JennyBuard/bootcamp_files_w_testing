function shortener (str){
	this.capitaliser = n => n[0].toUpperCase() + n.substring(1)
    this.surname     = this.capitaliser(str.split(' ')[1])
	this.name        = this.capitaliser(str.split(' ')[0])
    return this.name + ' ' +this.surname[0]+'.'
}
module.exports = {
    shortener
}
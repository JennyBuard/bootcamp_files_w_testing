var age = 35;
var end_age = 70;
var teas_day = 3;

var howManyTeas = function(age, end_age, teas_day){
    var days = 365;
    return (end_age - age) * (365 * teas_day)
}

module.exports = {
    howManyTeas,
    age,
    end_age,
    teas_day
}
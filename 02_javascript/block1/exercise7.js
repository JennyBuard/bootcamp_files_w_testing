    //tell which number is even
    //assign each of the below number to a variable, then using the module operator check which of them is even and which is odd, and console.log true if is even and false if is odd
    
    //3,  123 , 545 , 34 , 64 , 6634 , 876
var 
    a = 3, 
    b = 123, 
    c = 454, 
    d = 34, 
    e = 64, 
    f = 6634, 
    g = 876,
    arr = [ a, b , c , d , e , f , g ];

    var isEven = function (arr){
        var count = 0;
        arr.map(el =>{
            if(typeof el == 'number') el % 2 == 0 ? count++ : null
        })
        return count
    }

    module.exports = {
        isEven,
        arr
    }
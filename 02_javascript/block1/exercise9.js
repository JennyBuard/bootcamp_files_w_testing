    //using prompt :
    //get the name of the user and assign it to a variable called name.
    //get the year of birth of the user and assign it to a variable called birth_year
    //create a variable called no and assign it a value of the current year.
    //return his name and current age in a sentence like in the example:
    //                                                                 ******EXAMPLE******
    //Hello Jason you are 34 years old
    var name = "Jason", birth_year = 1982
    var now = 2018
    var getAge  = function (birth_year, now, name) {
        var age = now - birth_year;
        return `Hello ${name} you are ${age} years old`   
    }

    module.exports = {
        name, birth_year, getAge
    }
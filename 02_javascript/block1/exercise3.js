var date_of_birth = 1982
var future_year = 2018

var ageCalc = function(date_of_birth, future_year){
    return future_year - date_of_birth
}
module.exports = {
    ageCalc,
    date_of_birth,
    future_year,
}
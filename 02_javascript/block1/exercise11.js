    //It's hot out! Let's make a converter based on the steps here:
    http://www.mathsisfun.com/temperature-conversion.html
        //- Store a celsius temperature into a variable.
        //- Convert it to fahrenheit and output "NN°C is NN°F".
        //- Now store a fahrenheit temperature into a variable.
        //- Convert it to celsius and output "NN°F is NN°C."
var celsius = 35, fahrenheit = 120, c = "celsius", f = "fahrenheit"

var toCelsius = function (c, celsius){
    return Math.round(celsius * 9 / 5 + 32);
}
var toFahr = function (f, fahrenheit){
    return Math.round((fahrenheit -32) * 5 / 9);
}

module.exports = {
    celsius, 
    fahrenheit, 
    c,
    f,
    toCelsius, 
    toFahr,
}
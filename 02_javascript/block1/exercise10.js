    //get the user date of birth
    //assign the current date to a variable.
    //then return the following message, replacing the word *'DAYS'* with the actual value
    
    // you have lived for *'DAYS'* days already!
var now = 2018, dob = 1982
var howManyDays = function (dob, now){
    var days =  (now - dob) * 365 
    return `you have lived for ${days} days already!`
}
module.exports = {
    now, dob, howManyDays
}
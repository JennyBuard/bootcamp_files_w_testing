var obj = {a: 1, b: 2}
var modifyObject = (obj, key, val) =>{
    obj[key] = val;
    return obj
}
module.exports ={
    obj, modifyObject
}
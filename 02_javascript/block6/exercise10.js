var model = (action, obj, schema) => {
    var DB = []
	if (!action || action != "add") throw Error ("missing action argument or wrong provided")
    var newObj= {}
    for (let key in obj) {
        schema.includes(key) ? newObj[key] = obj[key] : null
    }
    DB.push(newObj)
	return DB;
}

module.exports = {
    model
}
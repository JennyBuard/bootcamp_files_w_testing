var runOnRange = obj => {
    var arr = []
    var {start, end, step} = obj;
	if (!step) return arr;
	if (start > end && step < 1) {
	 	for (var i = start; i >= end; i+=step){
        	arr.push(i)
    	}
    }
        for (var i = start; i <= end; i+=step){
            arr.push(i)
        }
    return arr
}

module.exports = {
    runOnRange
}
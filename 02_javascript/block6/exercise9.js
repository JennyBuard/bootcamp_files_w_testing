var sumAll = obj => {
    if(obj &&  Object.values(obj).length > 1){
        return Object.values(obj).reduce((a,b)=>a+b);
    }   return 0;
}
module.exports = {
    sumAll
}
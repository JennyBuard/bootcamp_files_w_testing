var model = (action, obj, schema) =>{
    var DB = []
    if (!action || action != 'add') throw Error ("missing action argument or wrong provided")
    var newObj = {}
    for(var key in obj){
        if(key in schema && typeof obj[key] == schema[key]){
            newObj[key] = obj[key]
        }
    }
    DB.push(newObj)
    return DB;
}
module.exports = {
    model
}

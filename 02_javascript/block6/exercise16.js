var obj = {d:3, z:10,a:11,c:55,b:3}
var sort = (obj, by, order) => {
    var sortAction = order+by
    var arr = [];
    for (var key in obj) {
        arr.push([key, obj[key]]);
    }
    var sorter = {
        ascendingvalues  : () => arr.sort((a, b) =>  a[1] - b[1]),
        descendingvalues : () => arr.sort((a, b) =>  b[1] - a[1]),
        ascendingkeys    : () => arr.sort(),
        descendingkeys   : () => arr.sort().reverse()
    }
    sorter[sortAction]()
    var newObj = {}
    arr.map(ele => newObj[ele[0]] = ele[1])
    return newObj;
}

module.exports ={
    sort
}
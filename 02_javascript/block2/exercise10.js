var arr = ['glass','car','watch','sofa','macbook']
var findPosition = (arr, ele)=> arr.indexOf(ele)

module.exports = {
    arr, findPosition
}
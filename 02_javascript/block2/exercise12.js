var arr = ['green','red','black','blue','brown','yellow','purple']
var isThere = (arr, ele)=> !arr.includes(ele)

module.exports ={
    arr, isThere
}
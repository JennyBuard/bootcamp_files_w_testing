var isArrayFunc = arr => Array.isArray(arr)
module.exports = {
    isArrayFunc
}
